# SOCKS Pyve
Smal SOCKS 5 proxy server made in [Python](https://www.python.org) with [asyncio](https://docs.python.org/3/library/asyncio.html).
Works with Python 3.6

Listen on every interface with port 1080 and create a log file after start.

## TODO
* add configuration file to edit listening addres, port, etc.
* add gssapi and username/password authentication methode
* add bind and udp associate request methode
