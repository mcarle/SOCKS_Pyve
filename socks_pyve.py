#!/usr/bin/env python
# -*- coding: UTF-8 -*-

__author__ = 'Marcel Carle'

import asyncio
import struct
import ipaddress
import socket
import time
import sys

class Host(asyncio.Protocol):
    '''
    connection to host with adress from client
    '''
    def __init__(self, log_file, server_transport):
        self.log_file = log_file
        self.server_transport = server_transport
        self.connected = False

    def connection_made(self, transport):
        self.peername = transport.get_extra_info('peername')
        # print(transport.get_extra_info('socket'))
        self.server_peername = self.server_transport.get_extra_info('peername')
        log_str = f'connection from {self.server_peername[0]} {self.server_peername[1]} to {self.peername[0]} {self.peername[1]} established'
        write_log(self.log_file, log_str)
        self.connected = True
        self.transport = transport

    def data_received(self, data):
        # data to host
        self.server_transport.write(data)

    def eof_received(self):
        pass

    def connection_lost(self, *args):
        log_str = f'connection from {self.server_peername[0]} {self.server_peername[1]} to {self.peername[0]} {self.peername[1]} lost'
        write_log(self.log_file, log_str)
        self.connected = False
        self.server_transport.close()


class Server(asyncio.Protocol):
    '''
    listen for client connection
    client <-> server
    create connection to host
    after connection to host is established
    client <-> server <-> host
    '''
    # SOCKS5 data from RFC
    SOCKS_VERSION = 0x05  # SOCKS Version 5
    ONE_AUTH_METHOD = 0x01  # Eine Auth Methode
    # auths methods
    NO_AUTH_REQUIRED = 0x00  # NO AUTHENTICATION REQUIRED
    GSS_API = 0x01  # GSSAPI
    USERNAME_PASSWORD = 0x02  # USERNAME and PASSWORD
    IANA_A = 0x03  # to X'7F' IANA ASSIGNED
    IANA_B = 0x7F
    # PRIMETHA = 0x80     # to X'FE' RESERVED FOR PRIVATE METHODS
    # PRIMETHB = 0xFE
    NO_ACC_METHODS = 0xFF  # NO ACCEPTABLE METHODS

    # Request
    CMD_CONNECT = 0x01  # CONNECT
    CMD_BIND = 0x02  # BIND
    CMD_UDP = 0x03  # UDP ASSOCIATE
    # ATYP   address type of following address
    ADRESS_TYPE_IP4 = 0x01  # IP V4 address
    ADRESS_TYPE_DOMAINNAME = 0x03  # DOMAINNAME
    ADRESS_TYPE_IP6 = 0x04  # IP V6 address

    # Reply field
    SUCCESS = 0x00  # succeeded
    GEN_SOCKS_FAIL = 0x01  # general SOCKS server failure
    CONECTION_NOT_ALLOWD = 0x02  # connection not allowed by ruleset
    NET_UNREACHABLE = 0x03  # Network unreachable
    HOST_UNREACHABLE = 0x04  # Host unreachable
    CONNECTION_REFUSED = 0x05  # Connection refused
    TTL_EXPIRED = 0x06  # TTL expired
    COMMAND_NOT_SUPORTED = 0x07  # Command not supported
    ADRESS_TYPE_NOT_SUPORTED = 0x08  # Address type not supported
    # UNASSIGNED        0x09    # to X'FF' unassigned
    RSV = 0x00  # (RSV) must be set to X'00'

    CLIENTS = {}

    def __init__(self, loop, log_file):
        self.loop = loop
        self.log_file = log_file
        self.requsts = 0
        self.host_adress = bytearray()
        self.host_port = 0
        self.host_socket = None
        self.host_protocol = None
        self.host = None

    def connection_made(self, transport):
        self.peername = transport.get_extra_info('peername')
        self.sockname = transport.get_extra_info('sockname')
        self.transport = transport
        self.sock = transport.get_extra_info('socket')
        self.CLIENTS[self.peername] = self.sock
        write_log(self.log_file, f'{len(self.CLIENTS)} connections')
        write_log(self.log_file, f'connection from {self.peername[0]} {self.peername[1]} established')

    @staticmethod
    def dom_req(data):
        domain_name_len = data[4]
        request = struct.unpack(f"!5B{domain_name_len}sH", data)
        domain_name = request[5]
        port = request[6]
        reply = struct.pack(f"!5B{domain_name_len}sH",
                          Server.SOCKS_VERSION, Server.SUCCESS, Server.RSV, Server.ADRESS_TYPE_DOMAINNAME, domain_name_len, domain_name, port)
        return bytearray(reply), domain_name, port

    @staticmethod
    def ipv4_req(data):
        request = struct.unpack('!4B4sH', data)
        ipv4_adress = str(ipaddress.IPv4Address(request[4]))
        port = request[5]
        reply = struct.pack('!4B4sH', Server.SOCKS_VERSION, Server.SUCCESS, Server.RSV, Server.ADRESS_TYPE_IP4, request[4], port)
        return bytearray(reply), ipv4_adress, port

    @staticmethod
    def ipv6_req(data):
        request = struct.unpack('!4B16sH', data)
        ipv6_adress = str(ipaddress.IPv6Address(request[4]))
        port = request[5]
        reply = struct.pack('!4B16sH', Server.SOCKS_VERSION, Server.SUCCESS, Server.RSV, Server.ADRESS_TYPE_IP6, request[4], port)
        return bytearray(reply), ipv6_adress, port

    async def send_data(self, data):
        reply = bytearray()
        if self.requsts == 0:
            self.requsts = 1
            methmsg = struct.unpack(f'!{len(data)}B', data)
            if self.NO_AUTH_REQUIRED in methmsg[2:]:
                reply = struct.pack('!2B', Server.SOCKS_VERSION, Server.NO_AUTH_REQUIRED)
                self.transport.write(reply)
            else:
                reply = struct.pack('!2B', Server.SOCKS_VERSION, Server.NO_ACC_METHODS)
                self.transport.write(reply)
                self.transport.close()
            return

        if self.requsts == 1:
            self.requsts = 2
            command = data[1]
            adress_type = data[3]

            if command != self.CMD_CONNECT:
                log_str = f'connection from {self.peername[0]} {self.peername[1]} error, only connect allowd'
                write_log(self.log_file, log_str)
                reply = bytearray(data)
                reply[1] = self.GEN_SOCKS_FAIL
                self.transport.write(reply)
                self.transport.close()
                return

            if adress_type == self.ADRESS_TYPE_DOMAINNAME:
                reply, self.host_adress, self.host_port = self.dom_req(data)
            elif adress_type == self.ADRESS_TYPE_IP4:
                reply, self.host_adress, self.host_port = self.ipv4_req(data)
            elif adress_type == self.ADRESS_TYPE_IP6:
                reply, self.host_adress, self.host_port = self.ipv6_req(data)

            try:
                self.host_socket = socket.create_connection((self.host_adress, self.host_port))
            except socket.error:
                log_str = f'connection from {self.peername[0]} {self.peername[1]} to {self.host_adress} {self.host_port} general SOCKS server failure'
                write_log(self.log_file, log_str)
                reply = bytearray(data)
                reply[1] = self.GEN_SOCKS_FAIL
                self.transport.write(reply)
                self.transport.close()
                return

            if adress_type == self.ADRESS_TYPE_DOMAINNAME:
                log_str = f'domain name {self.host_adress.decode()} is {self.host_socket.getpeername()[0]}'
                write_log(self.log_file, log_str)

            # make connection to host
            self.host_protocol, self.host = await self.loop.create_connection(lambda: Host(self.log_file, self.transport), sock=self.host_socket)
            self.transport.write(reply)
            return

        # data to client
        self.host_protocol.write(data)

    def data_received(self, data):
        # process data from client in a coroutine
        asyncio.create_task(self.send_data(data))

    def eof_received(self):
        pass

    def connection_lost(self, exc):
        write_log(self.log_file, f'connection from {self.peername[0]} {self.peername[1]} lost')
        del self.CLIENTS[self.peername]
        write_log(self.log_file, f'{len(self.CLIENTS)} connections')
        if self.host_protocol:
            if self.host.connected:
                self.host_protocol.close()


def write_log(log_file, log_msg):
    time_now = time.time()
    log_tm = time.strftime(f'%a, %d %b %Y %H:%M:%S.{str(time_now).split(".")[1]}: ', time.localtime(time_now))
    print(log_tm, log_msg, sep='', file=log_file)
    log_file.flush()


async def main():
    log_file_name = time.strftime(f'%H_%M_%S_{str(time.time()).split(".")[1]}.log', time.localtime())

    try:
        log_file = open(log_file_name, 'w')
    except:
        print("log file error")
        sys.exit(1)

    loop = asyncio.get_running_loop()
    server = await loop.create_server(lambda: Server(loop, log_file), None, 1080)

    server_host = server.sockets[0].getsockname()[0]
    server_port = server.sockets[0].getsockname()[1]
    write_log(log_file, f'listening on {server_host} {server_port}')

    try:
        async with server:
            await server.serve_forever()

    except KeyboardInterrupt:
        pass

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()
    log_file.close()


if __name__ == "__main__":
    asyncio.run(main())
